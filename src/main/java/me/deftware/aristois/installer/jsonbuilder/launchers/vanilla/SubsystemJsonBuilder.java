package me.deftware.aristois.installer.jsonbuilder.launchers.vanilla;

import com.google.gson.*;
import me.deftware.aristois.installer.InstallerAPI;
import me.deftware.aristois.installer.Main;
import me.deftware.aristois.installer.jsonbuilder.AbstractJsonBuilder;
import me.deftware.aristois.installer.utils.Utils;
import me.deftware.aristois.installer.utils.VersionData;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.file.Files;

/**
 * Minecraft versions 1.14 through 1.15.2
 * @author Deftware
 */
public class SubsystemJsonBuilder extends AbstractJsonBuilder {

	@Override
	public JsonObject build(VersionData data) {
		JsonObject json = new JsonObject();
		json.addProperty("inheritsFrom", data.getVersion());
		json.addProperty("id", data.getVersion() + "-Aristois");
		json.addProperty("time", getDate());
		json.addProperty("releaseTime", getDate());
		json.addProperty("type", "release");
		json.addProperty("mainClass", data.getMainClass());
		json.add("libraries", getLibraries(data, "vanilla"));
		return json;
	}

	private static final String asm = "9.0";

	public static JsonArray getLibraries(VersionData data, String launcher) {
		JsonArray libraries = new JsonArray();
		data.getLibraries().forEach(l -> libraries.add(generateMavenRepo(l.getName(), l.getUrl())));
		// EMC
		if (launcher.equalsIgnoreCase("multimc")) {
			libraries.add(generateMavenRepo("me.deftware:" + data.getEmc(), "https://gitlab.com/EMC-Framework/maven/raw/master/", "always-stale"));
			libraries.add(generateMavenRepo("me.deftware:subsystem:" + data.getSubsystem(), "https://gitlab.com/EMC-Framework/maven/raw/master/", "always-stale"));
		} else {
			libraries.add(generateMavenRepo("me.deftware:" + data.getEmc(), "https://gitlab.com/EMC-Framework/maven/raw/master/"));
			libraries.add(generateMavenRepo("me.deftware:subsystem:" + data.getSubsystem(), "https://gitlab.com/EMC-Framework/maven/raw/master/"));
		}
		// Fabric
		libraries.add(generateMavenRepo("net.fabricmc:sponge-mixin:" + data.getMixin(), "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("net.fabricmc:tiny-remapper:0.3.0.70", "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("net.fabricmc:tiny-mappings-parser:0.2.2.14", "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("net.fabricmc:fabric-loader-sat4j:2.3.5.4", "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("net.fabricmc:access-widener:1.0.0", "https://maven.fabricmc.net/"));
		// Other
		libraries.add(generateMavenRepo("com.google.jimfs:jimfs:1.2-fabric", "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("org.ow2.asm:asm:" + asm, "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("org.ow2.asm:asm-analysis:" + asm, "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("org.ow2.asm:asm-commons:" + asm, "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("org.ow2.asm:asm-tree:" + asm, "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("org.ow2.asm:asm-util:" + asm, "https://maven.fabricmc.net/"));
		libraries.add(generateMavenRepo("com.google.guava:guava:21.0", "https://maven.fabricmc.net/"));
		// Mappings
		libraries.add(generateMavenRepo("net.fabricmc:yarn:" + data.getMappings(), "https://maven.fabricmc.net/"));
		if (Utils.isMac()) {
			libraries.add(generateMavenRepo("com.thizzer.jtouchbar:jtouchbar:1.0.0", "https://repo1.maven.org/maven2/"));
		}
		return libraries;
	}

	@Override
	public String install(JsonObject json, VersionData data, String rootDir) {
		File parent = new File(rootDir + "versions" + File.separator + data.getVersion() + "-Aristois" + File.separator), jsonFile = new File(parent.getAbsolutePath() + File.separator + data.getVersion() + "-Aristois.json");
		if (!parent.exists() && !parent.mkdirs()) {
			InstallerAPI.getLogger().error("Failed to create {}", parent.getAbsolutePath());
		}
		if (jsonFile.exists()) {
			if (!jsonFile.delete()) {
				InstallerAPI.getLogger().error("Could not delete {}", jsonFile.getName());
			}
		}
		try (Writer writer = new FileWriter(jsonFile)) {
			new GsonBuilder().setPrettyPrinting().create().toJson(json, writer);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		addLauncherProfile(data, rootDir);
		return "Aristois has been installed, restart your Minecraft launcher and select \"release " + data.getVersion() + "-Aristois\" and hit play";
	}

	@Override
	public void addLauncherProfile(VersionData data, String rootDir) {
		JsonObject json = new JsonObject();
		json.addProperty("name", "Aristois " + data.getVersion());
		json.addProperty("type", "custom");
		json.addProperty("created", getDate("ms"));
		json.addProperty("lastUsed", getDate("ms"));
		json.addProperty("lastVersionId", data.getVersion() + "-Aristois");
		json.add("icon", new JsonPrimitive("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAQAAABpN6lAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfkCgwOJTHy1Kn6AAANk0lEQVR42u2de1BU1x3Hv+feu7vsLu+XPOQhCIj4QMUHisgbJE6atiatzautY8baNn1kUiemaZJJJ6mTmE5im8Q2adJMJ8m0nWkz8VWB+qIqoAiooKCACCLIe2FZdtm9/QMTYWHZu+fcu+DE31/Kcn/3dz57zrnfc36/ewDu2327b19nI8q5rgHRaOM5NaObUTQQ47x7EYCIS+t1nxJPRjfmkW0JnysXpqCU42+A8DUPkXB2T9aHYg+9at6lUJycUgD+jffCNblyeFKlvRD9nFJhKgcAWJgqxMvhR4iOWatclAoBeA4LNH6FnEYOX0Twy1+hfUkhAArNLiI+S1xxQCXT5G1pPv3AE7XKhKpIDxAARGcIUbL5i4hNU+q7UgTAJ/i+l3chkc034X3ycvVvKgJAEawivli18As+WD6PltazD2ypUSJYBXpAGAgJzeVkbD4ghEWlA/y9AeA4Xg7wzJf32yKcT+4jXp/cGwDmI3WZeqncXjUrvxP3yL0AIA2EDyrgvOX2y4fM3UAgi7BQFsBJvBOmy5E9ThDilftDn4OzHwCQlCokyO8V0Kx4MCFrtgPYiVjZJPCkUIPDMgjz6treZH6ylCI4PnIX76cEAAKO8F/sNn0gq1dZe4AAYF66EKlE8wFAvSxnYarMPmUF8Ake9/QuJEroFQAAHxCSSYi83UtWAA/j4UTNaqWaDwCeOc/6l85WAP4gJCyXm6MkAPWSjEULZyuAU3jJX24JbG+835wsIitjGQEkIHWZKlnR9gPQ5TwfcGY2AlgHwgcX8LJLYHtTL1q3NHo2AijFH0OVkMD2xnsHZxNevkSJjEMgaY0yEtje9Fm/DaqcbQCeRqjGv5DzcAcA9cKVyb6zDcBb2BPtscEdzQc4z8BcIqyYTQAEAPM2yLcL7Mz0ma/PqZhNAD7Go4pKYHtTJSQvl0tvyAJgCx5ZoKwEtgtaF5hLBHn2BmQA4AdCwnP4EPcBAHQZfwgrkcWTDOnxk/jM37NAWQk8Key4hSlokcOTDD0gCeuT1clubT84j4A8ot48GwCsBeGDCzgf9wIAtOl/Cv/HbABwwk0S2N6EmARZpl1mADySVgsL3A+AaHzzdJonZhrAU/BRu0sC25s2bW/EX5m9MD4F9iHdbRJ4UujR8am4SiAyeWHqATyAmHQhmt6D1Wwdob2WqPzy5nv8gqn5jAA+wnf1PgwSmOBWXUsVvYLQrP1N1B5GAExD4DF4JaoZNupFsbLcaIpeSVtLIkTOW4crHGwMbWDoAYEgCMtmkcDGnsMXS+qHu2mvJ4Jv/hrdywzNZwJQjF3+XgwSmOBW7bHGM023G+h9eKz5ScyvZwrAUmxIVi+jv95qrSo3thpaL1faqCdyITxmHVuCkxrAahA+KJ9FAg91HD6PLnSfrDL10vogvHf+Rv3bMwHgBPaG6BlqgQlu1JQ0oR+GosbuqwyDYNWTcTtmAoAai5kk8Ki5otzWBROGOzvrq+jFDB8WlQ7Qv5RACeAp6NR+hZyW9rYE/a0HqtGPUYyir7RypJ/aE/HO/Z7Xp+4GsA/7orUMElhEU2XFDQzyImwYKrrafY1BDqVsjv+WewEQALHrWSSwxVhajh6MWEGA4daOqyyDIGRuBgFtZ6QC8CE2630eINQqkqCn8UAt+mEFRMCC/lPnzQZ6b56523wOuRPAk3gsQbOGNmBAxJVzDTcxxI197TYMFtX3NDMMguWbEmnHIwUAPxCEZ/Oh9ABM/ccq0APzmIYnwHDjrUaGQcAFhWUQ4uUuAMewy99rI4sEvl1/8AoGYB37vwiY0VdWaR6i96jP/blvsbsALMGGpSwS2CZeKOu4BeM4hDYMFl3pa6GHqk7OSlrlHgDJYxLYlx6AsauoEn2w3O3yBDDWtTdXMzwJ/EOzCAl0B4DTeGuOjkkCt186cg2G8Yt4EbCgt6LSMkzv1zN7Z8ApdwDwwJJVqkT6QK2j584YOzFs192tGCy+PNDKMAiWbFgUpzyArRDU/kwS2NB+uBr94wfAnTCMVW0tNQxPAt/gbMK5XqTqIoD38WGkNoM6SgAt1ceuw+Bh11IbYEbvucpR6i1SQJ/9QuBpZQFwAGLTBYYKJctIWRm6YDJN/sgGQ3GtoY1+EKiSUpeGKQtgLzbpfQpZJHB/y4GLYxJ4UiAijGWtrRep2w/eOziH8IuUBLADW+M1LLvAuHauamwNOMlsgBk95ytHLfT+9VmvBbs6CFwA4AmCudm8673sKzMPnqgYWwNOaVYYSi4OtjM8CRYsX+bqCxUuACjBTj+WQgiC7msHau9KYHvjRRhP3rh5ifoGd+rHXFuluQBgFTKXqJfThyeirqK5HUOObmkFRsSe6krrKP099Bl7QlwbBJIBLAbhggpYXoYx9R49i94v14AOGAyUXBjqoL+HKn6piwWEkgGU4veMu8Adlw82YGC6PBYvwni85WYd/TDjdIG5RFWgBABvJK9kkcA2W3V5TwcZnq5xVmDE0nWx0mqV7HaSaTe8E+bK7pBEAE+CqP0LOR19YEOdRyrRJ1qciN1RGI7WGLvo7yPMT1zpyu9LBPARPmaSwARtF4oaJ64BpzKViKGjzR2XGQaBh38eUW+RFwAHIDZNiKEHMGqpKDffhtPlrgUwGbsunbMxZLy169+fK/09c0kA9mKjjk0CG24eGkuDSGCFgRM19ClzQIiJc0EKSAKwA9viNUxH2TSfP9WCQZWE1a6fiKGSps56BsGl9s0L1GyXD4AXCCKyWCSwxXSqHN0wSZH5vYCp7zZLyhzQrns96l35ABzBs36ehYT6KyHobXa0BpyaF/pPVtOnzAEhav4aqcejSACwBlmL2SRww9natq/SIFIuGCq+1sWQMicqv/xF2uflAbAQhAvOZ5HAIwPHKtCDEakTOwGGOzvrK1nq/zSpO6NfkQfAabw5R59HHwpB19WDlx2vASebCIyi/3/VJuqUOSBERq+TNgicAvBG8koVw+u6NtSWt01Mg0i5aPBIQw9Dypzwfvnput3sAB4FUQUwSeDh7uJz6IXZlQ5NAFNbx9XzTINg9fbYZ9kB/A0fRmoz6cMguFV7+KpzCTzR7qTMq+hT5oAQHp0mZY7nnH0Yl8ayC2y1VpUNdLo4AADABkNRfU8TwyDgvPMe9HSuBqYF8DYKdL6FREUPYOjW4aqJeUCJ4QOmxluNDNlCwGPVY3FPsQH4MbbFsUhggpYLJU0weLvcjrGU+RmGlDnAh0akOy+GngaAFkBEFs9wLOqoubzM1gXTAM3FzClzQrxzn/D+Fz2AEjzj67WRRQL33zhYI3ENOMXVMF5mSpkDmpRvxm+iB5CKnMVqhneURTSdP+sgDSLlaljQW86UMufnhGc6qx9zCCAJhJuTz/vT395iLC1Ht8M0iHOzYrC4buAGSybCK2e7bxEdgFLsCWaTwD2N06VBJJmx+ub1CyyDQL28MHHdtL/hcJL0xbKVqiT6W4u4cnasFO5LDXQbTdByvOR9aB3+bHt1oOr8kk0C9dlkfGBoJjnjK/a5CuBhEFX1RhYJbOo/Oq4UDgBiMICGx4XNUvuEDVuxRWMMpp+GAUCf88x7j/Y4Xs07APB3fBTBJoFvXzlYP3EAGEDU51L8nU3LE8OHJ+NrcerkzEXLTjj+fMoOSQDEpQmx9Le1iRfKOyevAfW9elc9sTXf+dFLUwJ4A9laHyYJbLxtXwo3Fo3VbadM3DXP7Of8HdePTTkEfomoeI/pJ89pjeBmrX0p3MyZenH6kpj/Ovp0ih6gBRCRySKBraOVZVOUws2QcT5B2YRzdMLLFAAO4Gc+bBLY0H6oanIp3MyZPuvFoDLpADJRsFidwnLDlurjzTBoZkv7oUpak+xoGpwEIAGEm5PHJIFHzpShGyaGkj+ZjfcKziH8YmkASvEGowTuu77/ggtpELeYPvN3weXSAARiWYrK1WK7cSbiWmVNKwYlp0HcYqoFy1dMfcqDHYBvg6gCNnIuy5W7Zh48We5KGsQ9xukDc4mQ7hzAP/HBXDYJ3H1tfx3jGlAR02W8GXrcOQAgPk01n/42IuoqrrdjaHYogPGmils65ZNtAoDdSNOy7QIP95acdTUN4h7jtAG5RPXQpJ9PkMK/Qsx8NgnccfmQk1K4mTNt+rvh2ye9nDeuB2gBRGbyc+lvYbPVlPV2zBYJbG/C/MQp3qsaB2A/nvbxYiiEAIY6/3OeJg3iHuM0fnmC+geOAWShYBGLBCZou1gsoRRu5kybti/iL44ARIJwIXl8AL37UUvFGSmlcDNnQkzcpLcdvgJQgd1BbBLY0EafBnGPEZVf3lyPn04NIBgpK1gkMNBcdVpiKdzMmcfaV6ImnjhyB8CDIEJAIcfw9yssw6fKpJbCzZwJUfaD4A6Az/F+hI5JAvc2H7gkZQ04s49IIvjmp2hfHI/ky39EpthCzEO0m7CEqysfK4Wb/hFgHjEPizO5TuC4xVsjf3Tl7ulTAgAQEPWmTp/XRqmrgUWx5jr6nK4BuddLwhpndpIQhhrIjnHHjwkAoIJFvd+IeoZzpUR0kz5n3y2xnmjHTG8UWQjA3wVAAICHVUOCQL8LQCBiQOwizjSgBwkCQ7pNBiOwoVfsuTtX3Z2TOKazBUWIEhQgAVHyz7zKGOl9+9rY/wHPgjNfV3nQwwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0xMC0xMlQxNDozNzo0OS0wNDowMAecem8AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMTAtMTJUMTQ6Mzc6NDktMDQ6MDB2wcLTAAAAAElFTkSuQmCC"));
		File profiles_json = new File(rootDir + "launcher_profiles.json"),
			launcher_ui_state = new File(rootDir + "launcher_ui_state.json");
		try {
			if (profiles_json.exists()) {
				JsonObject launcherJson = JsonParser.parseReader(Files.newBufferedReader(profiles_json.toPath())).getAsJsonObject();
				JsonObject profiles = launcherJson.get("profiles").getAsJsonObject();
				if (profiles.has("Aristois " + data.getVersion())) {
					profiles.remove("Aristois " + data.getVersion());
				}
				profiles.add("Aristois " + data.getVersion(), json);
				launcherJson.addProperty("selectedProfile", "Aristois " + data.getVersion());
				try (Writer writer = new FileWriter(profiles_json)) {
					new GsonBuilder().setPrettyPrinting().create().toJson(launcherJson, writer);
				}
			}
			// Set modding enabled
			if (launcher_ui_state.exists()) {
				JsonObject uiJson = JsonParser.parseReader(Files.newBufferedReader(launcher_ui_state.toPath())).getAsJsonObject();
				JsonObject jsonData = uiJson.get("data").getAsJsonObject();
				String uiSettings = jsonData.get("UiSettings").getAsString();
				jsonData.remove("UiSettings");
				// Update data
				if (uiSettings.contains("\"modded\":false")) {
					uiSettings = uiSettings.replace("\"modded\":false", "\"modded\":true");
					System.out.println("Modified modded variable");
				}
				jsonData.addProperty("UiSettings", uiSettings);
				try (Writer writer = new FileWriter(launcher_ui_state)) {
					new GsonBuilder().setPrettyPrinting().create().toJson(uiJson, writer);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
