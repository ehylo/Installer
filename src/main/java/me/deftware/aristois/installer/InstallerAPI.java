package me.deftware.aristois.installer;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.Getter;
import lombok.Setter;
import me.deftware.aristois.installer.utils.Utils;
import me.deftware.aristois.installer.utils.VersionData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class InstallerAPI {

	@Getter @Setter static boolean donorBuild = false, universal = false;
	@Getter static HashMap<String, VersionData> versions = new HashMap<>();
	@Getter static String dataUrl = "https://maven.aristois.net/versions.json", aristoisMaven = "https://maven.aristois.net/";
	@Getter static Logger logger = LogManager.getLogger("Aristois Installer");
	@Getter static JsonObject jsonData;

	public static void fetchData(boolean betaVersions) {
		try {
			getLogger().info("Fetching data from {}...", dataUrl);
			jsonData = new Gson().fromJson(Utils.get(dataUrl), JsonObject.class);
			populateVersions(betaVersions);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String[] getMinecraftVersions() {
		String[] data = new String[versions.size()];
		List<String> versions = new ArrayList<>();
		getVersions().keySet().forEach(k -> {
			if (k.split("\\.").length == 2) {
				k += ".0";
			}
			versions.add(k);
		});
		versions.sort(Comparator.comparingInt(value -> Integer.parseInt(value.replaceAll("\\.", ""))));
		Collections.reverse(versions);
		for (int i = 0; i < versions.size(); i++) {
			data[i] = "Minecraft " + versions.get(i).replace(".0", "");
		}
		return data;
	}

	public static void populateVersions(boolean betaVersions) {
		versions.clear();
		Arrays.stream(new Gson().fromJson(jsonData.get("versions").getAsJsonArray(), VersionData[].class)).forEach(v -> {
			if (!v.isBeta() || v.isBeta() && betaVersions) {
				versions.put(v.getVersion(), v);
			}
		});
	}

}
