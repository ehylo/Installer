package me.deftware.aristois.installer.modloader.impl;

import me.deftware.aristois.installer.InstallerAPI;
import me.deftware.aristois.installer.modloader.ModLoaderInstaller;
import me.deftware.aristois.installer.utils.Utils;
import me.deftware.aristois.installer.utils.VersionData;

import java.io.File;

/**
 * @author Deftware
 */
public class ForgeInstaller extends ModLoaderInstaller {

	@Override
	public String install(VersionData data, String rootDir) {
		String versionData = data.getVersion();
		if (versionData.split("\\.").length == 2) {
			versionData += ".0";
		}
		File modsDir = new File(Integer.parseInt(versionData.replaceAll("\\.", "")) < 1150 ? rootDir + "mods" + File.separator + data.getVersion() + File.separator :
				rootDir + "mods" + File.separator), emc = new File(modsDir.getAbsolutePath() + File.separator + "EMC.jar");
		if (!modsDir.exists() && !modsDir.mkdirs()) {
			InstallerAPI.getLogger().error("Could not create {}", modsDir.getAbsolutePath());
		}
		File emcDir = new File(rootDir + "libraries" + File.separator + "EMC" + File.separator + data.getVersion() + File.separator), aristois = new File(emcDir.getAbsolutePath() + File.separator + "Aristois.jar");
		if (!emcDir.exists() && !emcDir.mkdirs()) {
			InstallerAPI.getLogger().error("Could not create {}", emcDir.getAbsolutePath());
		}
		try {
			// EMC
			Utils.download(Utils.getMavenUrl(String.format("me.deftware:%s-Forge:%s", data.getEmc().split(":")[0].replace("-F-v2", ""), data.getEmc().split(":")[1]), "https://gitlab.com/EMC-Framework/maven/raw/master/"), emc);
			// Dependencies
			if (!data.getModLoaderDependencies().isEmpty()) {
				for (String dep : data.getModLoaderDependencies()) {
					File dependency = new File(modsDir.getAbsolutePath() + File.separator + dep.split("/")[dep.split("/").length - 1]);
					Utils.download(dep, dependency);
				}
			}
			// Aristois
			Utils.download(Utils.getMavenUrl(String.format("me.deftware:aristois%s:latest-%s", InstallerAPI.isDonorBuild() ? "-d" : "", data.getVersion()), "https://maven.aristois.net/"), aristois);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "Aristois for Forge has been installed, start your Forge instance and play";
	}

}
